export interface Corte {
    id?: number;
    _id?: number;
    infor_corte?: [];
    cantidad_inicial?: number;
    cantidad_final?: number;
}
