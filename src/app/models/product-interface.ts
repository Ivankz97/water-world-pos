export interface ProductInterface {
    id?: number;
    _id?: number;
    producto?: number;
    nombre?: string;
    descripcion?: string;
    precio_unitario?: string;
    unidad_de_medida?: string;
    total?: number;
    categoria?: string;
    cantidad?: number;
    foto?: File;
}
