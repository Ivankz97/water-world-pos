import { AuthService } from './../shared/services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { UserInterface } from './../models/user-interface';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    constructor(
      public router: Router,
      private authService: AuthService
    ) {}

    public user: UserInterface = {
        username: '',
        password: ''
    };

    ngOnInit() { }

    /*onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }*/

    ngOnDestroy() {
        //window.location.reload();
    }

    onLogin() {
        return this.authService.loginUser(this.user.username, this.user.password)
            .subscribe(result => {
                //console.log(result);
                //console.log(result.access);
                let token = result.access;
                this.authService.setToken(token);
                this.router.navigate(["/registro-corte"]);
            },
                error => Swal.fire({ type: 'error', 
                title: 'Error al iniciar sesión', text: 'El nombre de usuario o la contraseña son incorrectos.' })
            );
    }
}
