import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PageHeaderModule } from '../../../shared';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { LiquidarConsignaRoutingModule } from './liquidar-consigna-routing.module';
import { LiquidarConsignaComponent } from './liquidar-consigna.component';

@NgModule({
    imports: [CommonModule, LiquidarConsignaRoutingModule, ZXingScannerModule, PageHeaderModule, FormsModule],
    declarations: [LiquidarConsignaComponent]
})
export class LiquidarConsignaModule {}
