import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiquidarConsignaComponent } from './liquidar-consigna.component';

describe('LiquidarConsignaComponent', () => {
  let component: LiquidarConsignaComponent;
  let fixture: ComponentFixture<LiquidarConsignaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiquidarConsignaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiquidarConsignaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
