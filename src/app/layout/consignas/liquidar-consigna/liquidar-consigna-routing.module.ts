
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LiquidarConsignaComponent } from './liquidar-consigna.component';


const routes: Routes = [
    {
        path: '', component: LiquidarConsignaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LiquidarConsignaRoutingModule {
}
