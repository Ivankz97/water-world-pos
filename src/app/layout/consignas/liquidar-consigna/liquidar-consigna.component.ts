import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../shared/services/data-api.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';


@Component({
  selector: 'app-liquidar-consigna',
  templateUrl: './liquidar-consigna.component.html',
  styleUrls: ['./liquidar-consigna.component.scss']
})
export class LiquidarConsignaComponent implements OnInit {
  consigna: any;
  productos: any = [];

  montoEfectivo: number = 0;
  subtotal: number = 0;
  importe: number = 0;
  edicion: number = 1;

  newProducts: any = [];

  producto: any;

  efectivo: Boolean = false;
  vale: Boolean = false;

  cloneProducts: any = [];
  consignaPrueba: any = [
    {
      "cantidad": 2,
      "producto": 23,
      "metodo_pago": [
        {
          "metodo": "efectivo",
          "cantidad": 1
        },
        {
          "metodo": "efectivo",
          "cantidad": 1
        }
      ]
    }];

  constructor(private dataService: DataApiService, private route: Router) { }

  ngOnInit() {
    this.consigna = this.dataService.ObjConsignaEdit;
    console.log(this.consigna);
    if (this.consigna !== null) {
      this.productos = this.consigna.detalle_consigna;
    }
  }

  test() {
    if (this.efectivo) {
      this.producto.metodo_pago = 'efectivo';
    }
    if (this.vale) {
      this.producto.metodo_pago = 'vale';
    }
    this.producto.cantidad2 = this.montoEfectivo;
    console.log(this.producto.cantidad2);
    if (this.newProducts.length === 0) {
      this.newProducts.push(this.producto);
    } else {
      for (let index = 0; index < this.newProducts.length; index++) {
        if (this.producto.id !== this.newProducts[index].id) {
          console.log('HOla');
          this.newProducts.push(this.producto);
        }
      }
    }


    console.log(this.newProducts);
    console.log(this.productos);
    console.log(this.cloneProducts, "this good");
  }

  logItem(item) {
    console.log(item.cantidad);
    this.producto = item;
    this.cloneProducts = this.productos.slice();
    //this.test();
    /*if (this.newProducts.length === 0) {
      this.newProducts.push(item);
    } else {
      for (let i = 0; i < this.newProducts.length; i++) {
        if (item.id !== this.newProducts[i].id) {
          this.newProducts.push(item);
        } else {
          console.log('Fail');
        }
      }
    }*/
    //this.newProducts.push(this.producto);
  }

  calcularTotal() {
    let total = 0;
    for (let i = 0; i < this.productos.length; i++) {
      total = total + this.productos[i].cantidad * this.productos[i].precio_unitario;
    }
    return total;
  }

  pagoEfectivo() {
    this.efectivo = true;
    this.vale = false;
  }

  pagoVale() {
    this.vale = true;
    this.efectivo = false;
  }

  efectuarPago() {
    console.log(this.cloneProducts);
    //this.importe = this.calcularTotal();
    console.log(this.importe);
    this.dataService.updateConsigna(
      this.dataService.ObjConsignaEdit.id,
      this.cloneProducts,
      1,
      "hola",
      "prueba",
      this.importe,
      this.edicion
    ).subscribe(result => {
      Swal.fire({ type: 'success', title: '¡EDICIÓN COMPLETADA!', html: "Realizaste una edición con el" + "<br>" + "Nº de Consgina " + this.dataService.ObjConsignaEdit.id });
      this.route.navigate(['/administrar-consignas']);
    }, e => {
      Swal.fire({ type: 'error', title: 'Error', text: 'Ha ocurrido un error al intentar realizar la edición' });
    });
  }

}
