import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PageHeaderModule } from '../../../shared';
import { CrearConsignaRoutingModule } from './crear-consigna-routing.module';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { CrearConsignaComponent } from './crear-consigna.component';

@NgModule({
    imports: [CommonModule, CrearConsignaRoutingModule, ZXingScannerModule, PageHeaderModule, FormsModule],
    declarations: [CrearConsignaComponent]
})
export class CrearConsignaModule {}
