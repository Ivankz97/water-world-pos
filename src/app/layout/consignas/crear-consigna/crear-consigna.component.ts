import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../shared/services/data-api.service';
import { EmployeeInterface } from '../../../models/employee-interface';
import { ProductInterface } from '../../../models/product-interface';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-consigna',
  templateUrl: './crear-consigna.component.html',
  styleUrls: ['./crear-consigna.component.scss']
})
export class CrearConsignaComponent implements OnInit {

  stepOne: Boolean = false;
  stepTwo: Boolean = false;
  stepThree: Boolean = false;
  employees: any = [];
  assistants: any = [];
  disabled: Boolean = true;
  products: any = [];
  edit: Boolean = false;
  idProduct = null;
  subtotal: number = 0;
  cantidad: number = 0;
  contador: number = 1;
  items: number = 0;
  //test: Number = 0;
  productos: any = [];

  selectedProduct: ProductInterface = {
    nombre: '',
    descripcion: '',
    precio_unitario: '',
    unidad_de_medida: '',
    categoria: '',
    foto: null
  };

  public product: ProductInterface = {
    id: 0,
    producto: 0,
    nombre: '',
    descripcion: '',
    cantidad: 0,
    precio_unitario: '',
    total: 0,
    unidad_de_medida: '',
    categoria: '',
    foto: null
  };


  // Variables de prueba
  clearData: Boolean = false;
  montoEfectivo: number = 0;
  montoWaterCard: number = 0;
  entregarCliente: number = 0;
  moneda: any = "";
  saldo: number = 0;
  ventaRealizada: any;
  // Id del cliente de la watercard
  cliente: number = 0;
  watercoins: number = 0;
  array: any = [];

  pagos: Boolean = false;
  efectivo: Boolean = false;
  tarjeta: Boolean = false;

  detalle_venta: any = [];
  detalle_consigna: any = [];
  cantidad_watercard: number;
  cantidad_efectivo: number;
  total: number;
  efectivoPlusWaterCard: number = 0;
  mostrarCredito: number = 0;

  mostrarTotal: number = 0;
  user: any = "";

  stop: Boolean = false;
  disableWaterCard: Boolean = false;
  disabledPagoModal: Boolean = false;
  disableEfectivo: Boolean = false;
  contadorCantidad: number = 0;
  currency: string = 'mxn';

  // Variables para Código QR
  qrResultString: string;
  qrcodename: string;
  title = 'generate-qrcode';
  elementType: 'url' | 'canvas' | 'img' = 'url';
  value: string;
  display = false;
  href: string;
  waterCard: any;

  // Variables de prueba
  timeLeft: number = 60;
  interval;
  reporte: Boolean = false;
  datosReporte: any = [];
  reporteProductos: any = [];
  changeText: boolean;
  edicion: number = 1;

  ayudantes: any = [];
  fecha: any;
  showAyudantes: any;
  showWaterDetails: Boolean = false;

  repartidor: any;
  ruta: string = 'Prueba';
  estatus: string = "Consignada";
  importe: number = 0;

  employee: any;

  metodo_pago: any = [
    {
      "metodo": "Aqui hay un metodo",
      "cantidad": "1"
    }
  ];


  // Producto de prueba 
  testing: any = [
    {
      "id": 23,
      "nombre": "Garrafon",
      "descripcion": "Garrafon de agua de 19Lts",
      "precio_unitario": "15.00",
      "unidad_de_medida": "Pza",
      "categoria": "Bebida",
      "foto": "https://sistema.waterworld.com.mx/media/garrafon-de-pvc-para-agua-purificada-4980143z0-012735138.jpeg",
      "cantidad": 3,
      "producto": 23
    }];

  productoEditado: any = [
    {
      "id": 23,
      "nombre": "Garrafon",
      "descripcion": "Garrafon de agua de 19Lts",
      "precio_unitario": "15.00",
      "unidad_de_medida": "Pza",
      "categoria": "Bebida",
      "foto": "https://sistema.waterworld.com.mx/media/garrafon-de-pvc-para-agua-purificada-4980143z0-012735138.jpeg",
      "cantidad": 10,
      "producto": 23
    }];

  consignaPrueba: any = [
    {
      "cantidad": 2,
      "producto": 23,
      "metodo_pago": [
        {
          "metodo": "efectivo",
          "cantidad": 1
        },
        {
          "metodo": "Hi",
          "cantidad": 1
        }
      ]
    },
    {
      "cantidad": 10,
      "producto": 26,
      "metodo_pago": [
        {
          "metodo": "efectivo",
          "cantidad": 3
        },
        {
          "metodo": "efectivo",
          "cantidad": 4
        }
      ]
    }];

  constructor(private dataService: DataApiService, private route: Router) { }

  ngOnInit() {
    this.getListAssistants();
    this.empleadoActual();
    (document.getElementById('select-data') as HTMLButtonElement).disabled = true;
    (document.getElementById('confirm') as HTMLButtonElement).disabled = true;
    this.getListProducts();
    console.log(this.dataService.ObjConsignaEdit);
  }


  stepOneActivate() {
    this.stepOne = true;
    this.stepTwo = false;
    this.stepThree = false;
  }

  stepTwoActivate() {
    this.stepTwo = true;
    this.stepOne = false;
    this.stepThree = false;
  }

  stepThreeActivate() {
    this.stepThree = true;
    this.stepOne = false;
    this.stepTwo = false;
  }

  clickStepOneToTwo() {
    (document.getElementById('select-data') as HTMLButtonElement).disabled = false;
    (document.getElementById('confirm') as HTMLButtonElement).disabled = true;
    document.getElementById('select-data').click();
  }

  clickStepTwoToThree() {
    //(document.getElementById('select-data') as HTMLButtonElement).disabled = false;
    (document.getElementById('confirm') as HTMLButtonElement).disabled = false;
    document.getElementById('confirm').click();
    console.log(this.productos);
    this.array = this.productos;
  }


  getListAssistants() {
    this.dataService.getAllAssistants()
      .subscribe((employees: EmployeeInterface) => (this.employees = employees, console.log(this.employees)
      ));
  }

  empleadoActual() {
    this.dataService.getCurrentEmployee()
      .subscribe((employees: EmployeeInterface) => (this.employee = employees, console.log(this.employee)
      ));
  }

  /*crearConsigna() {
    this.dataService.postConsigna(
      this.detalle_consigna,
      this.repartidor,
      this.ruta,
      this.estatus,
      this.importe
      ).subscribe(result => {
        console.log(result);
      }, e => {
        console.log(e);
      });
  }*/

  findSso(employee) {
    var assistant;
    assistant = JSON.parse(employee);
    //this.ayudantes.ayudantes = assistant.id;
    //this.info_corte.push(this.ayudantes);
    if (this.assistants.length > 0) {
      for (let index = 0; index < this.assistants.length; index++) {
        if (this.assistants[index].id !== assistant.id) {
        } else {
          this.assistants.splice(index, 1);
        }
      }
    }
    this.assistants.push(assistant);
    console.log(this.assistants);
    if (this.assistants.length > 0) {
      this.disabled = false;
    } else {
      this.disabled = true;
    }
    //this.info_corte.push({ayudantes: assistant.id});
  }

  deleteAssistant(employee) {
    if (this.assistants.length > 0) {
      for (let index = 0; index < this.assistants.length; index++) {
        if (this.assistants[index].id !== employee.id) {
        } else {
          this.assistants.splice(index, 1);
        }
      }
    }
    if (this.assistants.length === 0) {
      this.disabled = true;
    }
  }

  getListProducts() {
    this.dataService.getAllProducts()
      .subscribe((products: ProductInterface) => (
        this.products = products));
  }

  addItem(item) {
    if (!item.cantidad) {
      item.cantidad = 1;
    } else {
    }
    if (this.productos.length > 0) {
      for (let index = 0; index < this.productos.length; index++) {
        if (this.productos[index].id !== item.id) {
        } else {
          this.contador++;
          // Funcionó
          item.cantidad = this.productos[index].cantidad;
          this.productos.splice(index, 1);
          item.cantidad = item.cantidad + 1;
        }
      }
    }
    item.producto = item.id;
    item.total = item.precio_unitario * item.cantidad;
    this.contador = item.cantidad;
    this.productos.push(item);
    this.subtotal = this.calcularTotal();
    this.items = this.totalItems();
  }

  editItems(product) {
    this.product = Object.assign({}, product);
    this.selectedProduct.nombre = product.nombre;
    this.selectedProduct.precio_unitario = product.precio_unitario;
    this.selectedProduct.foto = product.foto;
    this.cantidad = this.cantidad;
    this.idProduct = this.product.id;

  }

  calcularTotal() {
    let total = 0;
    for (let i = 0; i < this.productos.length; i++) {
      total = total + this.productos[i].cantidad * this.productos[i].precio_unitario;
    }
    return total;
  }

  totalItems() {
    let items = 0;
    for (let i = 0; i < this.productos.length; i++) {
      items = items + this.productos[i].cantidad;
    }
    return items;
  }

  accept() {
    this.cantidad = this.cantidad;
    this.product.cantidad = this.cantidad;
    for (let index = 0; index < this.productos.length; index++) {
      if (this.productos[index].id === this.product.id) {
        this.productos.splice(index, 1);
      }
    }
    this.product.total = +this.product.precio_unitario * this.product.cantidad;
    this.productos.push(this.product);
    this.idProduct = null;
    this.calcularTotal();
    this.subtotal = this.calcularTotal();
    this.items = this.totalItems();
    this.cantidad = 0;
  }

  clicked() {
    if (this.reporte === true) {
      console.log(this.reporteProductos);
    }
    var mywindow = window.open('', 'PRINT');

    //var mywindow = window.open('', 'PRINT', 'height=400,width=600');
    //mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    //mywindow.document.write('</head><body >');
    //mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById('print-section').innerHTML);

    //mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();
    /* Esta parte aun no funciona
    this.route.navigate(['/gestion-cobros']);
    this.startTimer();
    window.print();
    */

    //this.route.navigate(['/gestion-cobros']);
    return true;

    // Redirigir a la página del ticket
    // Llamar al método window.print()
    // Imprimir y cerrar ventana de impresión
  }

  registerAndPrint() {
    var id_repartidor = this.assistants[0].id;
    this.assistants[0].id
    if (this.dataService.editConsigna === true) {
      //console.log('hi', this.dataService.ObjConsignaEdit.importe);
      this.estatus = 'Editada';
      this.ruta = 'Aqui';
      this.importe = this.calcularTotal();
      //this.importe = this.calcularTotal();
      console.log(this.dataService.ObjConsignaEdit.id, this.productos, id_repartidor, this.ruta, this.estatus, this.importe);
      this.dataService.updateConsigna(
        this.dataService.ObjConsignaEdit.id,
        this.productos,
        id_repartidor,
        this.ruta,
        this.estatus,
        this.importe,
        this.edicion
      ).subscribe(result => {
        Swal.fire({ type: 'success', title: '¡EDICIÓN COMPLETADA!', html: "Realizaste una edición con el" + "<br>" + "Nº de Consgina " + this.dataService.ObjConsignaEdit.id });
        this.route.navigate(['/administrar-consignas']);
      }, e => {
        Swal.fire({ type: 'error', title: 'Error', text: 'Ha ocurrido un error al intentar realizar la edición' });
      });
    } else {
      this.importe = this.calcularTotal();
      this.dataService.postConsigna(
        this.productos,
        //this.metodo_pago,
        id_repartidor,
        this.ruta,
        this.estatus,
        this.importe
      ).subscribe(result => {
        console.log(result);
        Swal.fire({ type: 'success', title: '¡CONSIGNA AGREGADA!', html: "Realizaste una nueva consigna" + "<br>" + "Nº de Consigna " + result.id });
        this.route.navigate(['/administrar-consignas']);
      }, e => {
        console.log(this.productos, this.testing, id_repartidor, this.ruta, this.estatus, this.importe);
        Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Ha ocurrido un error al intentar crear la consigna' });
      });
    }
  }
}
