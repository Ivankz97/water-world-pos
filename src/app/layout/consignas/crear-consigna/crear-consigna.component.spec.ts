import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearConsignaComponent } from './crear-consigna.component';

describe('CrearConsignaComponent', () => {
  let component: CrearConsignaComponent;
  let fixture: ComponentFixture<CrearConsignaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearConsignaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearConsignaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
