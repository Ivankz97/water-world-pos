
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearConsignaComponent } from './crear-consigna.component';



const routes: Routes = [
    {
        path: '', component: CrearConsignaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CrearConsignaRoutingModule {
}
