import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrarConsignasComponent } from './administrar-consignas.component';

describe('AdministrarConsignasComponent', () => {
  let component: AdministrarConsignasComponent;
  let fixture: ComponentFixture<AdministrarConsignasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrarConsignasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrarConsignasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
