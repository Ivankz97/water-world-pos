import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataApiService } from '../../../shared/services/data-api.service';
import Swal from 'sweetalert2';
import { EmployeeInterface } from '../../../models/employee-interface';
import { ProductInterface } from '../../../models/product-interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-administrar-consignas',
  templateUrl: './administrar-consignas.component.html',
  styleUrls: ['./administrar-consignas.component.scss']
})
export class AdministrarConsignasComponent implements OnInit {

  consignas: any = [];
  employees: any = [];
  products: any = [];
  assistants: any = [];
  productos: any = [];
  contador: number = 0;
  subtotal: number = 0;
  items: number = 0;
  consignaAEditar: any;

  edit: Boolean = false;
  stepOne: Boolean = false;
  stepTwo: Boolean = false;
  stepThree: Boolean = false;
  disabled: Boolean = true;

  ticket: Boolean = false;
  consigna: any = [];
  ayudantes: any = [];
  fecha: any;
  showAyudantes: any;

  disableButton: Boolean = false;

  constructor(private dataService: DataApiService, private router: Router) { }

  ngOnInit() {
    this.getAllConsignas();
    this.getListAssistants();
    //(document.getElementById('select-data') as HTMLButtonElement).disabled = true;
    //(document.getElementById('confirm') as HTMLButtonElement).disabled = true;
    this.getListProducts();
    this.getAssistants();
  }

  ngOnDestroy() {
    this.dataService.ObjConsignaEdit = this.consignaAEditar;
  }

  getAllConsignas() {
    this.dataService.getConsignas().subscribe(result => {this.consignas = result, console.log(this.consignas);
    });
  }

  getListAssistants() {
    this.dataService.getAllAssistants()
      .subscribe((employees: EmployeeInterface) => (this.employees = employees, console.log(this.employees)
      ));
  }

  getListProducts() {
    this.dataService.getAllProducts()
      .subscribe((products: ProductInterface) => (
        this.products = products));
  }

  editConsigna(consigna) {
    console.log(consigna);
    this.consignaAEditar = consigna;
    console.log(this.consignaAEditar);
    Swal.fire({
      type: 'warning',
      title: '¿Seguro que deseas editar la consigna Nº ' + consigna.id + '?',
      text: "Recuerda que tienes solo 3 intentos para editar esta consigna",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, continuar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.edit = true;
        this.dataService.editConsigna = true;
        this.router.navigate(['/crear-consigna']);
        /*Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        );*/
      }
    });

    //Swal.fire({ type: 'success', title: '¡CONSIGNA AGREGADA!', html: "Realizaste una nueva consigna" + "<br>" + "Nº de Consgina 2839" });
  }


  stepOneActivate() {
    this.stepOne = true;
    this.stepTwo = false;
    this.stepThree = false;
  }

  stepTwoActivate() {
    this.stepTwo = true;
    this.stepOne = false;
    this.stepThree = false;
  }

  stepThreeActivate() {
    this.stepThree = true;
    this.stepOne = false;
    this.stepTwo = false;
  }

  clickStepOneToTwo() {
    (document.getElementById('select-data') as HTMLButtonElement).disabled = false;
    (document.getElementById('confirm') as HTMLButtonElement).disabled = false;
    document.getElementById('select-data').click();
  }

  clickStepTwoToThree() {
    //(document.getElementById('select-data') as HTMLButtonElement).disabled = false;
    (document.getElementById('confirm') as HTMLButtonElement).disabled = false;
    document.getElementById('confirm').click();
  }

  findSso(employee) {
    var assistant;
    assistant = JSON.parse(employee);
    //this.ayudantes.ayudantes = assistant.id;
    //this.info_corte.push(this.ayudantes);
    if (this.assistants.length > 0) {
      for (let index = 0; index < this.assistants.length; index++) {
        if (this.assistants[index].id !== assistant.id) {
        } else {
          this.assistants.splice(index, 1);
        }
      }
    }
    this.assistants.push(assistant);
    console.log(this.assistants);
    if (this.assistants.length > 0) {
      this.disabled = false;
    } else {
      this.disabled = true;
    }
    //this.info_corte.push({ayudantes: assistant.id});
  }

  addItem(item) {
    if (!item.cantidad) {
      item.cantidad = 1;
    } else {
    }
    if (this.productos.length > 0) {
      for (let index = 0; index < this.productos.length; index++) {
        if (this.productos[index].id !== item.id) {
        } else {
          this.contador++;
          // Funcionó
          item.cantidad = this.productos[index].cantidad;
          this.productos.splice(index, 1);
          item.cantidad = item.cantidad + 1;
        }
      }
    }
    item.producto = item.id;
    item.total = item.precio_unitario * item.cantidad;
    this.contador = item.cantidad;
    this.productos.push(item);
    this.subtotal = this.calcularTotal();
    this.items = this.totalItems();
  }

  calcularTotal() {
    let total = 0;
    for (let i = 0; i < this.productos.length; i++) {
      total = total + this.productos[i].cantidad * this.productos[i].producto.precio_unitario;
    }
    return total;
  }

  totalItems() {
    let items = 0;
    for (let i = 0; i < this.productos.length; i++) {
      items = items + this.productos[i].cantidad;
    }
    return items;
  }

  imprimirTicket(ticket) {
    this.ticket = true;
    //document.getElementById("print-section").style.visibility = "visible";
    console.log(ticket);
    this.consigna = ticket;
    this.productos = ticket.detalle_consigna;
    this.subtotal = this.calcularTotal();
  }

  getAssistants() {
    this.dataService.getCorte().subscribe((data) => (this.ayudantes = data, this.fecha = this.ayudantes.pop(), this.showAyudantes = this.fecha.info_corte));
    /*this.fecha = this.showAyudantes.fecha_corte.slice(0, 10);
    if (this.dataService.horaActual === this.fecha) {
      console.log('Igual', this.showAyudantes);
    } else {
      console.log('Nine');
    }*/
  }

  clicked() {
    var mywindow = window.open('', 'PRINT');
    //var mywindow = window.open('', 'PRINT', 'height=400,width=600');
    //mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    //mywindow.document.write('</head><body >');
    //mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById('print-section').innerHTML);

    //mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();
    /* Esta parte aun no funciona
    this.route.navigate(['/gestion-cobros']);
    this.startTimer();
    window.print();
    */
    return true;

    // Redirigir a la página del ticket
    // Llamar al método window.print()
    // Imprimir y cerrar ventana de impresión
  }

  liquidarConsigna(consigna) {
    this.consignaAEditar = consigna;
    console.log(this.dataService.ObjConsignaEdit);
    this.router.navigate(['/liquidar-consigna']);
  }

}
