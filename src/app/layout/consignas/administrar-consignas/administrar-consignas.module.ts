import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PageHeaderModule } from '../../../shared';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { AdministrarConsignasComponent } from './administrar-consignas.component';
import { AdministrarConsignasRoutingModule } from './administrar-consignas-routing.module';

@NgModule({
    imports: [CommonModule, AdministrarConsignasRoutingModule, ZXingScannerModule, PageHeaderModule, FormsModule],
    declarations: [AdministrarConsignasComponent]
})
export class AdministrarConsignasModule {}
