
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministrarConsignasComponent } from './administrar-consignas.component';



const routes: Routes = [
    {
        path: '', component: AdministrarConsignasComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdministrarConsignasRoutingModule {
}
