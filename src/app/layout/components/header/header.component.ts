import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {formatDate, DatePipe } from '@angular/common';
import { DataApiService } from 'src/app/shared/services/data-api.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    providers: [DatePipe]
})
export class HeaderComponent implements OnInit {
    public pushRightClass: string;
    date: number = Date.now();
    day: any;
    month: any; 
    year: any;
    hour: any;
    today = new Date();
    jstoday = '';
    user: any = "hola";
    test: any;

    constructor(private translate: TranslateService, public router: Router, public dataApiService: DataApiService, private datePipe: DatePipe) {

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
        setInterval(() => {
            this.date = Date.now();
            this.test = this.date;
            this.test = this.datePipe.transform(this.test, 'h:mm a');
            //console.log(this.date = Date.now());
            this.dataApiService.horaActual = this.test;
            //console.log(this.dataApiService.horaActual);
            //console.log(this.datePipe.transform(this.date, 'dd-MM-yyyy'));
            // date | date:'dd-MM-yyyy
        }, 1000);
    }

    ngOnInit() {
        this.pushRightClass = 'push-right';
        this.currentUser();
        /*this.day = new Date().getDate();
        this.month = new Date().getMonth();
        this.year = new Date().getFullYear();
        this.hour = new Date().getHours();
        console.log(this.month);
        console.log(this.jstoday);*/
    }

    currentUser() {
        this.dataApiService.getCurrentUser().subscribe((user) => (this.user = user));
        this.dataApiService.currentUser = this.user;
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        localStorage.removeItem('accessToken');
        sessionStorage.removeItem('accessToken');
        localStorage.clear();
        sessionStorage.clear();
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    addAssistant() {
        this.dataApiService.addAssistant = true;
    }
}
