import { CorteRoutingModule } from './corte-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageHeaderModule } from './../../shared';
import { RegistroCorteComponent } from './registro-corte.component';

@NgModule({
    imports: [CommonModule, CorteRoutingModule, PageHeaderModule],
    declarations: [RegistroCorteComponent]
})
export class RegistroCorteModule {}
