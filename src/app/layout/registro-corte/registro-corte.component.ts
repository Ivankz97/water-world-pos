import { EmployeeInterface } from './../../models/employee-interface';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataApiService } from 'src/app/shared/services/data-api.service';

@Component({
  selector: 'app-registro-corte',
  templateUrl: './registro-corte.component.html',
  styleUrls: ['./registro-corte.component.scss']
})
export class RegistroCorteComponent implements OnInit {
  employees: EmployeeInterface;
  disabled: Boolean = true;
  
  assistants: any = [];
  inventoryItems: any = [];
  info_corte: any = [];
ayudantes = {
    ayudantes: ""
  };



  user: any;

  constructor(private router: Router, public dataApiService: DataApiService) { }

  public employee: EmployeeInterface = {
    id: 0,
    nombre: '',
    apellidos: '',
    telefono: null,
    fechaNacimiento: null,
    direccion: null,
    seguroSocial: true,
    tipoEmpleado: '',
    planta: '',
    sucursal: '',
    fechaIngreso: null,
    estatus: ''
  };

  ngOnInit() {
    this.getListAssistants();
    this.getInventory();
    //this.jala = true;
    this.desactivarSideBar();
    this.currentUser();
    console.log(this.dataApiService.currentUser);
  }

  ngOnDestroy() {
    this.dataApiService.sidebar = false;
  }

  currentUser() {
    this.dataApiService.getCurrentUser().subscribe((user) => (this.user = user, console.log(this.user)
    ));
    this.dataApiService.currentUser = this.user;
  }

  desactivarSideBar() {
    this.dataApiService.sidebar = true;
  }

  getListAssistants() {
    this.dataApiService.getAllAssistants()
      .subscribe((employees: EmployeeInterface) => (this.employees = employees, console.log(this.employees)
      ));
  }

  getInventory() {
    this.dataApiService.getAllProductsInventory()
    .subscribe((inventory: []) => (this.inventoryItems = inventory, console.log(this.inventoryItems)
    ));
  }

  addAssistant(employee) {
    if (this.assistants.length > 0) {
      for (let index = 0; index < this.assistants.length; index++) {
        if (this.assistants[index].id !== employee.id) {
        } else {
          this.assistants.splice(index, 1);
        }
      }
    }
    this.assistants.push(employee.id);
    console.log(this.assistants);
    console.log(this.assistants);
    if (this.assistants.length > 0) {
      this.disabled = false;
    } else {
      this.disabled = true;
    }
    console.log(this.assistants);
  }

  deleteAssistant(employee) {
    if (this.assistants.length > 0) {
      for (let index = 0; index < this.assistants.length; index++) {
        if (this.assistants[index].id !== employee.id) {
        } else {
          this.assistants.splice(index, 1);
        }
      }
    }
    if (this.assistants.length === 0) {
      this.disabled = true;
    }
  }

  findSso(employee) {
    var assistant;
    assistant = JSON.parse(employee);
    //this.ayudantes.ayudantes = assistant.id;
    //this.info_corte.push(this.ayudantes);
    if (this.assistants.length > 0) {
      for (let index = 0; index < this.assistants.length; index++) {
        if (this.assistants[index].id !== assistant.id) {
        } else {
          this.assistants.splice(index, 1);
        }
      }
    }
    this.assistants.push(assistant);
    if (this.assistants.length > 0) {
      this.disabled = false;
    } else {
      this.disabled = true;
    }
    this.info_corte.push({ayudantes: assistant.id});
    /*console.log(this.info_corte[0].ayudantes);
    for (let i = 0; i < this.assistants.length; i++) {
      if (this.employee.id !== this.info_corte[i].ayudantes) {
        console.log("Entra");
      } else {
        console.log("Repetido");
      }
    }*/
    //this.ayudantes.push(this.assistants);
  }


  postAssistants() {
    this.dataApiService.postAssistants(this.info_corte, 0, 0).subscribe(
      data => {
        console.log(data);
      });
  }
}
