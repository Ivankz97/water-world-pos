import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistroCorteComponent } from './registro-corte.component';


const routes: Routes = [
    {
        path: '', component: RegistroCorteComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CorteRoutingModule {
}
