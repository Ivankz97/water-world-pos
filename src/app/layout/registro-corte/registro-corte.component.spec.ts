import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroCorteComponent } from './registro-corte.component';

describe('RegistroCorteComponent', () => {
  let component: RegistroCorteComponent;
  let fixture: ComponentFixture<RegistroCorteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroCorteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroCorteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
