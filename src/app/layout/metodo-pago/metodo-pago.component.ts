import { DataApiService } from 'src/app/shared/services/data-api.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-metodo-pago',
  templateUrl: './metodo-pago.component.html',
  styleUrls: ['./metodo-pago.component.scss']
})
export class MetodoPagoComponent implements OnInit {
  products: any = [];
  subtotal: number;
  clearData: Boolean = false;
  montoEfectivo: number = 0;
  montoWaterCard: number = 0;
  entregarCliente: number = 0;
  moneda: any = "";
  saldo: number = 0;
  ventaRealizada: any;
  // Id del cliente de la watercard
  cliente: number = 0;
  watercoins: number = 0;

  pagos: Boolean = false;
  efectivo: Boolean = false;
  tarjeta: Boolean = false;

  detalle_venta: any = [];
  cantidad_watercard: number;
  cantidad_efectivo: number;
  total: number;
  efectivoPlusWaterCard: number = 0;
  mostrarCredito: number = 0;

  mostrarTotal: number = 0;
  user: any = "";

  validacionCantidad: Boolean = false;
  validacionWaterCard: Boolean = false;
  validacionDolar: Boolean = false;
  stop: Boolean = false;
  disableWaterCard: Boolean = false;
  disabledPagoModal: Boolean = false;
  disableEfectivo: Boolean = false;
  contadorCantidad: number = 0;
  currency: string = 'mxn';

  // Variables para Código QR
  qrResultString: string;
  qrcodename : string;
  title = 'generate-qrcode';
  elementType: 'url' | 'canvas' | 'img' = 'url';
  value: string;
  display = false;
  href: string;
  waterCard: any;

  // Variables de prueba
  timeLeft: number = 60;
  interval;
  reporte: Boolean = false;
  datosReporte: any = [];
  reporteProductos: any = [];
  changeText: boolean;

  ayudantes: any = [];
  fecha: any;
  showAyudantes: any;
  showWaterDetails: Boolean = false;

  valorDolar: number;
  valorCreditos: number;

  constructor(public dataService: DataApiService, public route: Router) {
    this.changeText = false;
  }

  ngOnInit() {
    this.products = this.dataService.productos;
    console.log(this.products);
    this.subtotal = this.dataService.subtotal;
    this.detalle_venta = this.products;
    this.total = this.subtotal;
    this.currentUser();
    this.getCurrentExchange();
    console.log(this.dataService.recargaTarjeta);
    console.log(this.detalle_venta);
    this.getAssistants();
    this.getPrices();
  }

  ngOnDestroy() {
    if (this.clearData === true) {
      this.dataService.productos = this.products = [];
      this.dataService.subtotal = 0;
    } else {
      this.dataService.productos = this.products;
      this.dataService.subtotal = this.subtotal;
    }
  }

  getPrices() {
    this.dataService.getAllPrices().subscribe(
      result => {
        this.valorDolar = result[0].dolar;
        this.valorCreditos = result[0].valor_creditos;
        console.log(this.valorDolar, this.valorCreditos);
      },
      e => {
        console.log('Un error ha ocurrido.');
      }
    );
  }

  cancel() {
    //this.location.back(); // <-- go back to previous location on cancel
    window.history.back();
  }


  currentUser() {
    this.dataService.getCurrentUser().subscribe((user) => (this.user = user
    ));
  }

  calcularEntrega() {
    this.mostrarTotal = this.subtotal - this.montoEfectivo;
    if (this.mostrarTotal < 0) {
      this.mostrarTotal = this.mostrarTotal * -1;
    } else {
      this.mostrarTotal = 0;
    }

    if (this.currency === 'dolar') {
      this.mostrarTotal = this.subtotal - (this.montoEfectivo * 20); // Valor del dólar
      if (this.mostrarTotal < 0) {
        this.mostrarTotal = this.mostrarTotal * -1;
      }
    }
  }

  calcularCreditos() {
    this.mostrarCredito = this.cantidad_watercard - this.waterCard.saldo;
    if (this.montoWaterCard * 10 > this.subtotal) {
      this.disabledPagoModal = true;
    } else {
      this.disabledPagoModal = false;
    }
  }

  metodoPago() {
    this.validacionCantidad = false;
    this.mostrarTotal = this.subtotal - this.montoEfectivo;
    this.pagos = true;
    if (this.montoEfectivo > 0) {
      this.cantidad_efectivo = this.montoEfectivo;
      this.cantidad_watercard = this.montoWaterCard;
      if (this.subtotal - this.montoEfectivo > 0) {
        this.entregarCliente = this.subtotal - this.montoEfectivo;
      } else {
        this.entregarCliente = (this.subtotal - this.montoEfectivo) * -1;
      }
      this.efectivo = true;
    }
    if (this.entregarCliente < 0) {
      this.entregarCliente = this.entregarCliente * -1;
    }
    if (this.tarjeta === true) {
      this.entregarCliente = this.entregarCliente - this.montoWaterCard;
    }
    if (this.products.length === 0) {
      this.detalle_venta = null;
    }
    for (let i = 0; i < this.products.length; i++) {
      if (this.products[i].cantidad === 0) {
        this.validacionCantidad = true;
      } else {
        this.contadorCantidad++;
      }
    }
    if (this.currency === 'dolar') {
      //this.cantidad_efectivo = this.cantidad_efectivo * 20;
      this.cantidad_efectivo = this.cantidad_efectivo * this.valorDolar;

      //this.cantidad_efectivo = this.cantidad_efectivo * this.moneda.rates.MXN;
      this.montoEfectivo = this.cantidad_efectivo;
      console.log(this.cantidad_efectivo);
      /*if (this.montoEfectivo > this.total) {
        this.validacionDolar = true;
      }*/
      this.entregarCliente = this.cantidad_efectivo - this.subtotal;
    }
    if (this.montoWaterCard > 0 && this.tarjeta === true) {
      this.efectivoPlusWaterCard = this.montoWaterCard + this.montoEfectivo;
      if (this.efectivoPlusWaterCard >= this.total) {
        this.validacionWaterCard = false;
        this.entregarCliente = this.entregarCliente * -1;
      }
    }
    if (this.montoEfectivo >= this.subtotal) {
      this.disableWaterCard = true;
    }
    if (this.mostrarTotal < 0) {
      this.mostrarTotal = this.mostrarTotal * -1;
    }

  }

  metodoWaterCard() {
    this.watercoins = this.montoWaterCard;
    console.log(this.watercoins);
    this.pagos = true;
    this.tarjeta = true;
    this.saldo = this.saldo - this.montoWaterCard;
    //this.montoWaterCard = this.montoWaterCard * 10;
    this.montoWaterCard = this.montoWaterCard * this.valorCreditos;
    this.cantidad_efectivo = this.montoEfectivo;
    this.cantidad_watercard = this.montoWaterCard;
    /*if (this.montoWaterCard >= this.subtotal && this.efectivo === false) {
      this.cantidad_watercard = this.montoWaterCard;
      this.entregarCliente = this.subtotal - this.montoEfectivo - this.montoWaterCard;
    }*/
    if (this.entregarCliente > 0) {
      this.entregarCliente = this.subtotal - this.montoEfectivo - this.montoWaterCard;
    } else {
      this.entregarCliente = (this.subtotal - this.montoWaterCard) * -1;
    }
    if (this.entregarCliente < 0) {
      this.entregarCliente = this.entregarCliente * -1;
    }
    if (this.products.length === 0) {
      this.detalle_venta = null;
    }
    if (this.montoWaterCard < this.subtotal && this.efectivo === false) {
      this.validacionWaterCard = true;
    }
    if (this.cantidad_watercard >= this.subtotal) {
      this.disableEfectivo = true;
    }
    this.showWaterDetails = true;
  }

  devolucionEfectivo() {
    this.subtotal = this.subtotal;
    this.montoEfectivo = 0;
    this.cantidad_efectivo = null;
    this.efectivo = false;
    this.disableWaterCard = false;
  }

  devolucionWaterCard() {
    this.subtotal = this.subtotal;
    this.montoWaterCard = 0;
    this.cantidad_watercard = null;
    this.tarjeta = false;
    this.disableEfectivo = false;
    this.saldo = this.saldo + this.watercoins;
    console.log(this.saldo, this.watercoins);
  }

  onRegisterSale(): void {
    if (this.validacionCantidad === true  && this.contadorCantidad === 0 || 
      this.validacionWaterCard === true || this.stop === true
      ) {
      Swal.fire({ type: 'error', title: 'Error', text: 'No es posible generar la venta.' });
    } else {
      this.dataService.postSale(
        this.detalle_venta,
        this.cantidad_watercard,
        this.cantidad_efectivo,
        this.total
      )
        .subscribe(sale => {
          console.log(sale);
          this.datosReporte.push(sale);
          this.reporte = true;
          if (this.tarjeta === true) {
            console.log(this.qrResultString, this.cliente, this.saldo);
            this.updateWaterCard2(this.qrResultString, this.cliente, this.saldo);
          }
          if (this.dataService.recargaTarjeta === true) {
            console.log(this.dataService.waterCodigo, this.dataService.idClienteCard, this.dataService.actualizarSaldo);
            this.updateWaterCard2(this.dataService.waterCodigo, this.dataService.idClienteCard, this.dataService.actualizarSaldo);
          }
          if (this.dataService.ventaTarjeta === true) {
            //console.log(this.dataService.idClienteCard, this.dataService.waterCodigo, this.dataService.defaultWaterSaldo);
            this.postWaterCard(this.dataService.idClienteCard, this.dataService.waterCodigo, this.dataService.defaultWaterSaldo);
          }
          Swal.fire({
            title: 'Venta completada',
            html: "Total venta: $" + this.subtotal + "<br>" + "Cambio: $" + this.entregarCliente,
            type: 'success',
            confirmButtonColor: '#3674D9',
            confirmButtonText: 'Imprimir ticket',
            confirmButtonClass: 'printSectionId="print-section" ngxPrint>print',
            allowOutsideClick: false,
            onClose: () => {
              this.clicked();
              this.showWaterDetails = false;
              console.log(this.qrResultString);
              //console.log(this.dataService.productos);
            },
          });
          this.ventaRealizada = sale;
          this.reporteProductos = this.dataService.productos;
          this.dataService.productos = null;
          this.dataService.ventaTarjeta = false;
          this.dataService.subtotal = 0;
          this.dataService.recargaTarjeta = false;
          this.clearData = true;
          this.subtotal = 0;
          this.montoEfectivo = 0;
          this.pagos = false;
          this.efectivo = false;
          this.tarjeta = false;
          this.disableWaterCard = false;
          this.disableEfectivo = false;
          this.ngOnInit();
        },
          e => {
            Swal.fire({ type: 'error', title: 'Error del servidor', text: 'Ha ocurrido un error al intentar realizar la venta.' })
          });
    }
  }

  onCodeResult(resultString: string) {
    this.qrResultString = resultString;
    console.log(this.qrResultString);
    //alert(this.qrResultString);
    this.getWaterCardId(this.qrResultString);
  }

  generateQRCode(){
    if (this.qrcodename === '') {
      this.display = false;
      alert("Please enter the name");
      return;
    } else {
      this.value = this.qrcodename;
      this.display = true;
    }
  }

  downloadImage() {
    this.href = document.getElementsByTagName('img')[0].src;
  }

  radioChangeHandler(event: any) {
    this.currency = event.target.value;
    console.log(this.currency);
  }

  changeToDollars() {
    if (this.currency === 'dolar') {
      this.cantidad_efectivo = this.cantidad_efectivo * this.moneda.rates.MXN;
      console.log(this.cantidad_efectivo);
      return this.cantidad_efectivo;
    }
  }

  getCurrentExchange() {
    this.dataService.getCurrentExchange().subscribe((moneda) => (this.moneda = moneda, console.log(this.moneda.rates.MXN)));
  }

  getWaterCardId(id) {
    this.dataService.getWaterCard(id).subscribe((card) => (this.waterCard = card, this.saldo = this.waterCard.saldo,
       this.cliente = this.waterCard.cliente.id, // Se añadio el id del cliente para realizar el update
       console.log(this.waterCard, this.saldo, this.cliente)));
  }

  updateWaterCard(id, cliente, saldo) {
    this.dataService.putWaterCard(
      this.waterCard.id,
      this.cliente,
      this.saldo
    ).subscribe(data => {
    });
  }

  postWaterCard(cliente, codigo, saldo) {
    this.dataService.postWaterCard(
      cliente,
      codigo,
      saldo
    ).subscribe(data => {
      console.log("Works");
    }, e => {
      console.log("Failed");
    });
  }

  getAssistants() {
    this.dataService.getCorte().subscribe((data) => (this.ayudantes = data, this.fecha = this.ayudantes.pop(), this.showAyudantes = this.fecha.info_corte, console.log(this.fecha.fecha_corte.slice(0, 10), this.fecha)));
    /*this.fecha = this.showAyudantes.fecha_corte.slice(0, 10);
    if (this.dataService.horaActual === this.fecha) {
      console.log('Igual', this.showAyudantes);
    } else {
      console.log('Nine');
    }*/
  }

  updateWaterCard2(codigo, cliente, saldo) {
    this.dataService.putWaterCard(
      codigo,
      cliente,
      saldo
    ).subscribe(data => {
    });
  }
  clicked() {
    if (this.reporte === true) {
      console.log(this.reporteProductos);
    }
    var mywindow = window.open('', 'PRINT');

    //var mywindow = window.open('', 'PRINT', 'height=400,width=600');
    //mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    //mywindow.document.write('</head><body >');
    //mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById('print-section').innerHTML);

    //mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();
    /* Esta parte aun no funciona
    this.route.navigate(['/gestion-cobros']);
    this.startTimer();
    window.print();
    */

    this.route.navigate(['/gestion-cobros']);
    return true;

    // Redirigir a la página del ticket
    // Llamar al método window.print()
    // Imprimir y cerrar ventana de impresión
  }

  /*over() {
    console.log("Mouseover called");
  }*/


  /*
  prueba() {
    let test, fecha;
    test = this.ayudantes.pop();
    fecha = test.fecha_corte.slice(0, 10);
    if (this.dataService.horaActual === fecha) {
      console.log('Igual', test);
    } else {
      console.log('Nine');
    }
  }
  */
}
