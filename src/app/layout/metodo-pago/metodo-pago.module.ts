import { MetodoPagoComponent } from './metodo-pago.component';
import { MetodoPagoRoutingModule } from './metodo-pago-routing.module';

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PageHeaderModule } from '../../shared';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import {NgxPrintModule} from 'ngx-print';


@NgModule({
    imports: [CommonModule, MetodoPagoRoutingModule, ZXingScannerModule, NgxQRCodeModule, NgxPrintModule, PageHeaderModule, FormsModule],
    declarations: [MetodoPagoComponent]
})
export class MetodoPagoModule {}
