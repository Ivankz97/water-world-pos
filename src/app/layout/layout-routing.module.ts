//import { RegistroCorteComponent } from './registro-corte/registro-corte.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        //redirectTo: './dashboard/dashboard.module#DashboardModule',
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'registro-corte', loadChildren: './registro-corte/registro-corte.module#RegistroCorteModule' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'gestion-cobros', loadChildren: './gestion-cobros/gestion-cobros.module#GestionCobrosModule'},
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'metodo-pago', loadChildren: './metodo-pago/metodo-pago.module#MetodoPagoModule'},
            { path: 'historial-tickets', loadChildren: './historial-tickets/historial-tickets.module#HistorialTicketsModule'},
            { path: 'registrar-cliente', loadChildren: './registrar-cliente/registrar-cliente.module#RegistrarClienteModule'},
            { path: 'crear-consigna', loadChildren: './consignas/crear-consigna/crear-consigna.module#CrearConsignaModule'},
            { path: 'administrar-consignas', loadChildren: './consignas/administrar-consignas/administrar-consignas.module#AdministrarConsignasModule'},
            { path: 'liquidar-consigna', loadChildren: './consignas/liquidar-consigna/liquidar-consigna.module#LiquidarConsignaModule'}
            //{ path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            //{ path: 'forms', loadChildren: './form/form.module#FormModule' },
            //{ path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
           // { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            //{ path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' }
        ],
    }/*,
    {
        path: 'registro-corte', component: RegistroCorteComponent
    }*/
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
