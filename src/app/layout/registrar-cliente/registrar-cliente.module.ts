import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PageHeaderModule } from '../../shared';
import { RegistrarClienteComponent } from './registrar-cliente.component';
import { RegistrarClienteRoutingModule } from './registrar-cliente-routing.module';


@NgModule({
    imports: [CommonModule, RegistrarClienteRoutingModule, PageHeaderModule, FormsModule],
    declarations: [RegistrarClienteComponent]
})
export class RegistrarClienteModule {}
