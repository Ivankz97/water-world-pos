import { Component, OnInit } from '@angular/core';
import { CustomerInterface } from '../../models/customer-interface';
import { DataApiService } from '../../shared/services/data-api.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-cliente',
  templateUrl: './registrar-cliente.component.html',
  styleUrls: ['./registrar-cliente.component.scss']
})
export class RegistrarClienteComponent implements OnInit {
  customers: CustomerInterface;
  edit: Boolean = false;
  constructor(private router: Router, private dataApi: DataApiService) { }
  public customer: CustomerInterface = {
    id: 0,
    nombre: "",
    contacto: "",
    zona: "",
    telefono: null,
    estatus: true,
  };

  ngOnInit() {
    this.getListCustomers();
  }


  getListCustomers() {
    this.dataApi.getAllCustomers().subscribe((customers: CustomerInterface) => (this.customers = customers));
  }

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.dataApi.selectedCustomer = null;
    }
    this.edit = false;
  }

  preEditCustomer(customer: CustomerInterface) {
    this.customer = Object.assign({}, customer);
    window.scrollTo(0, 0);
    this.edit = true;
    console.log(this.edit);
  }

  addCustomer(form?: NgForm) {
    console.log(form.value);
    if (form.value._id) {
      this.dataApi.putCustomer(form.value)
        .subscribe(res => {
          this.resetForm(form);
          this.getListCustomers();
          console.log("editado");
          Swal.fire({ type: 'success', title: 'Editar', text: 'El cliente ha sido editado satisfactoriamente.' })
        },
        e => {
          Swal.fire({ type: 'error', title: 'Edición inválida', text: 'Datos del formulario incompletos' })
        });
    } else {
      this.dataApi.postCustomer(form.value)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Registro', text: 'El cliente ha sido registrado satisfactoriamente.' })
          this.resetForm(form);
          console.log(res);
          //this.getListCustomers();
        },
        e => {
          Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
        });
    }
  }

}
