import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistorialTicketsComponent } from './historial-tickets.component';



const routes: Routes = [
    {
        path: '', component: HistorialTicketsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HistorialTicketsRoutingModule {
}
