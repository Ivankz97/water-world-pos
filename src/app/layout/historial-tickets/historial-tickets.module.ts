

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PageHeaderModule } from '../../shared';
import { HistorialTicketsComponent } from './historial-tickets.component';
import { HistorialTicketsRoutingModule } from './historial-tickets-routing.module';


@NgModule({
    imports: [CommonModule, HistorialTicketsRoutingModule, PageHeaderModule, FormsModule],
    declarations: [HistorialTicketsComponent]
})
export class HistorialTicketsModule {}
