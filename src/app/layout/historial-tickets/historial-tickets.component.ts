import { DataApiService } from 'src/app/shared/services/data-api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-historial-tickets',
  templateUrl: './historial-tickets.component.html',
  styleUrls: ['./historial-tickets.component.scss']
})
export class HistorialTicketsComponent implements OnInit {
  ventas: any;
  mostrarVenta: any = [];
  venta: any;
  productos: any = [];
  primerTicket: any = [];
  items: number = 0;

  constructor(public dataService: DataApiService) { }

  ngOnInit() {
    this.Sales();
  }

  Sales() {
    this.dataService.getSales().subscribe((sales) => (
      this.ventas = sales, console.log(this.ventas), this.primerTicket = this.ventas[0], console.log(this.primerTicket)
    ))
    ;
  }

  showSale(sale) {
    //this.items = 0;
    let salee: any = [];
    console.log(sale);
    this.totalItems2(sale);
    if (this.mostrarVenta.length === 0) {
      this.items = 0;
      this.mostrarVenta.push(sale);
      this.venta = this.mostrarVenta[0];
      this.productos = this.venta.detalle_venta;
      this.items = this.totalItems();
    } else {
      this.mostrarVenta = [];
      this.mostrarVenta.push(sale);
      this.venta = this.mostrarVenta[0];
      this.productos = this.venta.detalle_venta;
      this.items = this.totalItems();
    }
  }

  totalItems() {
    let items = 0;
    for (let i = 0; i < this.productos.length; i++) {
      items = items + this.productos[i].cantidad;
      console.log(items);
    }
    return items;
  }

  totalItems2(sale) {
    let items = 0;
    for (let i = 0; i < sale.length; i++) {
      items = items + sale[i].cantidad;
      console.log(sale);
    }
    return items;
  }


}
