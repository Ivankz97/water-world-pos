import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import Swal from 'sweetalert2';
import { DataApiService } from 'src/app/shared/services/data-api.service';
import { ProductInterface } from 'src/app/models/product-interface';
import { CustomerInterface } from '../../models/customer-interface';


@Component({
  selector: 'app-gestion-cobros',
  templateUrl: './gestion-cobros.component.html',
  styleUrls: ['./gestion-cobros.component.scss']
})
export class GestionCobrosComponent implements OnInit {
  customers: CustomerInterface;

  usuarioRegistrado: any;
  edit: Boolean = false;
  idProduct = null;
  subtotal: number = 0;
  cantidad: number = 0;
  contador: number = 1;
  items: number = 0;
  //test: Number = 0;
  productos: any = [];
  //pi: number = 3.14;
  //e: number = 2;
  qrResultString: string;
  qrResult: string;
  qrcodename: string;
  waterCard: any;
  saldo: number = 0;
  // Id del cliente de la watercard
  cliente: number = 0;

  registeredUser: string = 'No';
  registered: Boolean = false;
  camaraActiva: Boolean = false;

  editarProductos: any = [];
  products: ProductInterface;
  selectedProduct: ProductInterface = {
    nombre: '',
    descripcion: '',
    precio_unitario: '',
    unidad_de_medida: '',
    categoria: '',
    foto: null
  };

  public customer: CustomerInterface = {
    id: 0,
    nombre: "",
    contacto: "",
    zona: "",
    telefono: null,
    estatus: true,
  };

  creditos: number = 0;
  error: Boolean = false;
  updateCard: Boolean = false;
  showProducts: any = [];

  recargarTarjeta = {
    idTarjeta: '0',
    recargar: 0,
    creditosActuales: 0
  };

  constructor(private router: Router, private dataApiService: DataApiService) { }

  public product: ProductInterface = {
    id: 0,
    producto: 0,
    nombre: '',
    descripcion: '',
    cantidad: 0,
    precio_unitario: '',
    total: 0,
    unidad_de_medida: '',
    categoria: '',
    foto: null
  };

  public watercard: ProductInterface = {
    id: 0,
    producto: 0,
    nombre: '',
    descripcion: '',
    cantidad: 0,
    precio_unitario: '',
    total: 0,
    unidad_de_medida: '',
    categoria: '',
    foto: null
  };

  ngOnInit() {
    this.getListProducts();
    this.productos = this.dataApiService.productos;
    this.subtotal = this.dataApiService.subtotal;
    this.items = this.totalItems();
    console.log(this.productos);
    this.getListCustomers();
  }

  ngOnDestroy() {
    this.dataApiService.productos = this.productos;
    this.dataApiService.subtotal = this.subtotal;
  }

  getListProducts() {
    this.dataApiService.getAllProducts()
      .subscribe((products: ProductInterface) => (
        this.products = products, this.showProducts = this.products,
        this.filterArray(this.showProducts)
        ));
  }

  filterArray(products) {
    for (let i = 0; i < products.length; i++) {
      if (products[i].nombre === 'Tarjeta Prepago' || products[i].nombre === 'Recargar Tarjeta') {
        products.pop(products[i]);
        console.log('hola');
      }
      console.log(products);
    }
  }

  addItem(item) {
    if (!item.cantidad) {
      item.cantidad = 1;
    } else {
    }
    if (this.productos.length > 0) {
      for (let index = 0; index < this.productos.length; index++) {
        if (this.productos[index].id !== item.id) {
        } else {
          this.contador++;
          // Funcionó
          item.cantidad = this.productos[index].cantidad;
          this.productos.splice(index, 1);
          item.cantidad = item.cantidad + 1;
        }
      }
    }
    item.producto = item.id;
    item.total = item.precio_unitario * item.cantidad;
    this.contador = item.cantidad;
    this.productos.push(item);
    this.subtotal = this.calcularTotal();
    this.items = this.totalItems();
  }

  editItems(product) {
    this.product = Object.assign({}, product);
    this.selectedProduct.nombre = product.nombre;
    this.selectedProduct.precio_unitario = product.precio_unitario;
    this.selectedProduct.foto = product.foto;
    this.cantidad = this.cantidad;
    this.idProduct = this.product.id;

  }

  accept() {
    this.cantidad = this.cantidad;
    this.product.cantidad = this.cantidad;
    for (let index = 0; index < this.productos.length; index++) {
      if (this.productos[index].id === this.product.id) {
        this.productos.splice(index, 1);
      }
    }
    this.product.total = +this.product.precio_unitario * this.product.cantidad;
    this.productos.push(this.product);
    this.idProduct = null;
    this.calcularTotal();
    this.subtotal = this.calcularTotal();
    this.items = this.totalItems();
    this.cantidad = 0;
  }

  calcularTotal() {
    let total = 0;
    for (let i = 0; i < this.productos.length; i++) {
      total = total + this.productos[i].cantidad * this.productos[i].precio_unitario;
    }
    return total;
  }

  totalItems() {
    let items = 0;
    for (let i = 0; i < this.productos.length; i++) {
      items = items + this.productos[i].cantidad;
    }
    return items;
  }

  onCodeResult(resultString: string) {
    this.qrResultString = resultString;
    console.log(this.qrResultString);
    this.dataApiService.waterCodigo = this.qrResultString;
    this.getWaterCardId(this.qrResultString);
    /*if (this.updateCard === true) {
      this.getWaterCardId(this.qrResultString);
    }*/
    this.updateCard = false;
    console.log('Entro aqui');
  }

  actualizarCard(resultString: string) {
    this.qrResultString = 'empty';
    this.qrResult = resultString;
    console.log(this.qrResult);
    this.dataApiService.waterCodigo = this.qrResult;
    //alert(this.qrResultString);
    if (this.camaraActiva === false) {
      this.getWaterCardId(this.qrResult);
    }
    /*if (this.updateCard === true) {
      this.getWaterCardId(this.qrResultString);
    }*/
    this.updateCard = false;
  }

  getWaterCardId(id) {
    this.dataApiService.getWaterCard(id).subscribe((card) => (this.waterCard = card, this.saldo = this.waterCard.saldo,
       this.cliente = this.waterCard.cliente, this.dataApiService.waterCardId = this.waterCard.id,
       console.log(this.waterCard, this.saldo, this.cliente, this.dataApiService.waterCardId)),
       e => {
         this.error = true;
         console.log(this.error);
         this.qrResultString = '';
         this.error = false;
       });
  }

  createWaterCard() {
    this.watercard.id = 29;
    this.watercard.nombre = 'Tarjeta Prepago',
    this.watercard.categoria = 'Tarjeta',
    this.watercard.cantidad = 1,
    this.watercard.descripcion = 'Watercard',
    this.watercard.foto = null,
    this.watercard.precio_unitario = '117';
    if (this.usuarioRegistrado) {
      this.dataApiService.idClienteCard = this.usuarioRegistrado.id;
    } else {
     this.dataApiService.idClienteCard = this.cliente;
    }
    this.addItem(this.watercard);
    this.dataApiService.ventaTarjeta = true;
    console.log(this.watercard);
  }

  recargarWaterCard() {
    this.product.id = 30;
    this.product.nombre = 'Recargar Tarjeta',
    this.product.cantidad = 1,
    this.product.descripcion = 'Recargar Water card',
    this.product.foto = null,
    this.product.precio_unitario = this.creditos.toString();
    this.addItem(this.product);
    console.log(this.product);
    this.dataApiService.recargaTarjeta = true;
    this.dataApiService.actualizarSaldo = +this.creditos + +this.waterCard.saldo;
    console.log(this.waterCard.cliente.id);
    this.dataApiService.idClienteCard = this.waterCard.cliente.id;
    this.dataApiService.waterCodigo = this.waterCard.codigo;
  }

  logCreditos() {
    console.log(this.creditos);
    this.recargarTarjeta.idTarjeta = this.qrResult;
    this.recargarTarjeta.recargar = this.creditos;
    this.recargarTarjeta.creditosActuales = this.saldo;
    console.log(this.recargarTarjeta);
    this.recargarWaterCard();
  }

  getListCustomers() {
    this.dataApiService.getAllCustomers().subscribe((customers: CustomerInterface) => (this.customers = customers));
  }

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.dataApiService.selectedCustomer = null;
    }
    this.edit = false;
  }

  preEditCustomer(customer: CustomerInterface) {
    this.customer = Object.assign({}, customer);
    window.scrollTo(0, 0);
    this.edit = true;
    console.log(this.edit);
  }

  addCustomer(form?: NgForm) {
    console.log(form.value);
    if (form.value._id) {
      this.dataApiService.putCustomer(form.value)
        .subscribe(res => {
          this.resetForm(form);
          this.getListCustomers();
          Swal.fire({ type: 'success', title: 'Editar', text: 'El cliente ha sido editado satisfactoriamente.' })
        },
        e => {
          Swal.fire({ type: 'error', title: 'Edición inválida', text: 'Datos del formulario incompletos' })
        });
    } else {
      this.dataApiService.postCustomer(form.value)
        .subscribe(res => {
          this.usuarioRegistrado = res;
          Swal.fire({ type: 'success', title: 'Registro', text: 'El cliente ha sido registrado satisfactoriamente.', 
          showConfirmButton: false, timer: 1000 });
          this.dataApiService.idCliente = this.usuarioRegistrado.id;
          console.log(this.usuarioRegistrado.id, 'Id usuario');
          this.resetForm(form);
          this.registeredUser = 'No';
          this.registered = true;
          this.camaraActiva = true;
          console.log(res);
          //this.getListCustomers();
        },
        e => {
          Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
        });
    }
  }

  radioChangeHandler(event: any) {
    this.registeredUser = event.target.value;
  }

  activaCamara() {
    this.camaraActiva = true;
    this.registered = true;
  }

  enableUpdate() {
    this.updateCard = true;
  }

  testEdit(product) {
    console.log(product);
    console.log(this.productos);
    for (let i = 0; i < this.productos.length; i++) {
      if (product.id === this.productos[i].id) {
        this.productos.splice( this.productos.id, 1 );
      }
    }
  }

}
