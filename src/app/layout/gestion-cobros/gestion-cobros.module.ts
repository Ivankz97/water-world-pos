import { GestionCobrosComponent } from './gestion-cobros.component';

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PageHeaderModule } from '../../shared';
import { GestionCobrosRoutingModule } from './gestion-cobros-routing.module';
import { ZXingScannerModule } from '@zxing/ngx-scanner';

@NgModule({
    imports: [CommonModule, GestionCobrosRoutingModule, ZXingScannerModule, PageHeaderModule, FormsModule],
    declarations: [GestionCobrosComponent]
})
export class GestionCobrosModule {}
