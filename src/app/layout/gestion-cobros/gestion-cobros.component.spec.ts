import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionCobrosComponent } from './gestion-cobros.component';

describe('GestionCobrosComponent', () => {
  let component: GestionCobrosComponent;
  let fixture: ComponentFixture<GestionCobrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionCobrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionCobrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
