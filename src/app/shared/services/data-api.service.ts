import { Sales } from './../../models/sale-interface';
import { UserInterface } from './../../models/user-interface';
import { AuthService } from './auth.service';
import { StockInterface } from './../../models/stock-interface';
import { EmployeeInterface } from './../../models/employee-interface';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs/internal/Observable";
import { map } from "rxjs/operators";
import { CustomerInterface } from 'src/app/models/customer-interface';
import { PlantInterface } from 'src/app/models/plant-interface';
import { ProductInterface } from 'src/app/models/product-interface';
import { Corte } from '../../models/corte-inteface';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  headers : HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
    "Authorization": "Bearer " + this.authService.getToken(),
  });

  productos: any = [];
  subtotal = 0;
  currentUser: any;

  defaultWaterSaldo = 10;
  waterCodigo: string;

  sidebar: Boolean = false;

  // Variables para la recarga de water cards
  // Si esta variable es true, entonces se deberá hacer una recarga
  recargaTarjeta: Boolean = false;
  // Id water card para activar y/o vender tarjeta
  waterCardId: any;
  actualizarSaldo: number = 0;
  idClienteCard: number = 0;
  idCliente: number = 0;
  ventaTarjeta: Boolean = false;
  addAssistant: Boolean = false;
  
  public selectedCustomer: CustomerInterface = {
    _id: null,
    nombre: "",
    contacto: "",
    zona: "",
    telefono: null,
    estatus: false
  };

  horaActual: number;
  editConsigna: Boolean = false;
  ObjConsignaEdit: any = null;


  url_api = "https://sistema.waterworld.com.mx/api/";
  //url_prod = "https://sistema.waterworld.com.mx/api/token/";

  getAllCustomers(){
    //const url_api = "http://localhost:8000/api/clientes/";
    return this.http.get(this.url_api + "clientes/", {headers: this.headers});
  }

  /*************************  RUTAS PARA ClIENTES  *******************************/
  registerCustomer(nombre: string, contacto: string, zona: string, telefono: number, estatus: Boolean){
    return this.http.post<CustomerInterface>(this.url_api + "clientes/", {
      nombre: nombre,
      contacto: contacto,
      zona: zona,
      telefono: telefono,
      estatus: estatus
    }, {headers: this.headers}
    ).pipe(map(data=>data));
  }

  postCustomer(customer: CustomerInterface) {
    return this.http.post(this.url_api, customer, {headers: this.headers});
  }

  putCustomer(customer: CustomerInterface) {
    return this.http.put(this.url_api + `${customer._id}/`, customer, {headers: this.headers});
  }

  deleteCustomer(_id: number) {
    return this.http.delete(this.url_api + `${_id}/`, {headers: this.headers});
  }

  /*************************  RUTAS PARA PLANTAS  *******************************/
  getAllPlants(){
    const url_api_plants = "http://localhost:8000/api/plantas/";
    return this.http.get(url_api_plants, {headers: this.headers});
  }

  postPlant(plant: PlantInterface){
    const url_api_plants = "http://localhost:8000/api/plantas/";
    return this.http.post(url_api_plants, plant, {headers: this.headers});
  }

  putPlant(plant: PlantInterface) {
    const url_api_plants = "http://localhost:8000/api/plantas/";
    return this.http.put(url_api_plants + `${plant._id}/`, plant, {headers: this.headers});
  }

  deletePlant(_id: number) {
    const url_api_plants = "http://localhost:8000/api/plantas/";
    return this.http.delete(url_api_plants + `${_id}/`, {headers: this.headers});
  }

  /*************************  RUTAS PARA SUCURSALES  *******************************/
  getAllBranchOffices(){
    const url_api_branch_offices = "http://localhost:8000/api/sucursales/";
    return this.http.get(url_api_branch_offices, {headers: this.headers});
  }

  postBranchOffice(plant: PlantInterface){
    const url_api_branch_offices = "http://localhost:8000/api/sucursales/";
    return this.http.post(url_api_branch_offices, plant, {headers: this.headers});
  }

  putBranchOfffice(plant: PlantInterface) {
    const url_api_branch_offices = "http://localhost:8000/api/sucursales/";
    return this.http.put(url_api_branch_offices + `${plant._id}/`, plant, {headers: this.headers});
  }

  deleteBranchOffice(_id: number) {
    const url_api_branch_offices = "http://localhost:8000/api/sucursales/";
    return this.http.delete(url_api_branch_offices + `${_id}/`, {headers: this.headers});
  }

  /*************************  RUTAS PARA EMPLEADOS  *******************************/
  getAllEmployees(){
    const url_api_employees = "http://localhost:8000/api/empleados/";
    return this.http.get(url_api_employees, {headers: this.headers});
  }

  postEmployee(employee: EmployeeInterface){
    const url_api_employees = "http://localhost:8000/api/empleados/";
    return this.http.post(url_api_employees, employee, {headers: this.headers});
  }

  putEmployee(employee: EmployeeInterface) {
    const url_api_employees = "http://localhost:8000/api/empleados/";
    return this.http.put(url_api_employees + `${employee._id}/`, employee, {headers: this.headers});
  }

  deleteEmployee(_id: number) {
    const url_api_employees = "http://localhost:8000/api/empleados/";
    return this.http.delete(url_api_employees + `${_id}/`, {headers: this.headers});
  }

  /*************************  RUTAS PARA PLANTAS  *******************************/
  getAllUsers(){
    const url_api_users = "http://localhost:8000/api/auth/register/";
    return this.http.get(url_api_users, {headers: this.headers});
  }

  postUser(user: UserInterface){
    const url_api_users = "http://localhost:8000/api/auth/register/";
    return this.http.post(url_api_users, user, {headers: this.headers});
  }

  putUser(user: UserInterface) {
    const url_api_users = "http://localhost:8000/api/auth/register/";
    return this.http.put(url_api_users + `${user._id}/`, user, {headers: this.headers});
  }

  deleteUser(_id: number) {
    const url_api_users = "http://localhost:8000/api/auth/register/";
    return this.http.delete(url_api_users + `${_id}/`, {headers: this.headers});
  }

  /*************************  RUTAS PARA PRODUCTOS  *******************************/
  getAllProducts(){
    return this.http.get(this.url_api + "productos/", {headers: this.headers});
  }

  postProduct(product: ProductInterface){
    return this.http.post(this.url_api + "productos/", product, {headers: this.headers});
  }

  putProduct(product: ProductInterface) {
    return this.http.put(this.url_api + "productos/" + `${product._id}/`, product, {headers: this.headers});
  }

  deleteProduct(_id: number) {
    return this.http.delete(this.url_api + "productos/" + `${_id}/`, {headers: this.headers});
  }


  /*************************  RUTAS PARA INVENTARIO  *******************************/
  getAllProductsInventory(){
    return this.http.get(this.url_api + "inventario/", {headers: this.headers});
  }

  postProductInventory(stock: StockInterface){
    return this.http.post(this.url_api + "inventario/", stock, {headers: this.headers});
  }

  putProductInventory(stock: StockInterface) {
    return this.http.put(this.url_api + "inventario/" + `${stock._id}/`, stock, {headers: this.headers});
  }

  deleteProductInventory(_id: number) {
    return this.http.delete(this.url_api + "inventario/" + `${_id}/`, {headers: this.headers});
  }

  /*************************  RUTAS PARA VENTAS  *******************************/
  postSale(detalle_venta: [], cantidad_watercard: number, cantidad_efectivo: number, total: number) {
    //const url_api = "http://localhost:8000/api/ventas/";
    return this.http.post<Sales>(this.url_api + "ventas/", {
      detalle_venta: detalle_venta,
      cantidad_watercard: cantidad_watercard,
      cantidad_efectivo: cantidad_efectivo,
      total: total
    }, {headers: this.headers}
    ).pipe(map(data => data));
  }

  getSales(){
    //const url_api = "http://localhost:8000/api/ventas/";
    return this.http.get(this.url_api + "ventas/", {headers: this.headers});
  }

  /*************************  RUTAS PARA OBTENER USUARIO ACTUAL  *******************************/
  getCurrentUser(){
    return this.http.get(this.url_api + "currentuser/", {headers: this.headers});
  }

  /*************************  RUTAS PARA AYUDANTES  *******************************/
  getAllAssistants(){
    //const url_api_stock = "http://localhost:8000/api/ayudantes/";
    return this.http.get(this.url_api + "ayudantes/", {headers: this.headers});
  }

  getCurrentExchange(){
    const url_exchanges = "https://api.exchangeratesapi.io/latest?base=USD";
    return this.http.get(url_exchanges);
  }

  postAssistants(info_corte: any, cantidad_inicial: number, cantidad_final: number) {
    //const url_api_assistants = "http://localhost:8000/api/corte/";
    return this.http.post(this.url_api + "corte/", {
      info_corte: info_corte,
      cantidad_inicial: cantidad_inicial,
      cantidad_final: cantidad_final
    }, {headers: this.headers});
  }

  getCorte(){
    //const url_api_stock = "http://localhost:8000/api/corte/";
    return this.http.get(this.url_api + "corte/", {headers: this.headers});
  }

  /*************************  RUTAS PARA WATER CARDS  *******************************/

  getWaterCard(_id: number) {
    //const url_api_watercard = "http://localhost:8000/api/watercard/";
    return this.http.get(this.url_api + "watercard/" + `${_id}/`, {headers: this.headers});
  }

  postWaterCard(_idCliente: number, codigo: string, saldo: number) {
    //const url_api_watercard = "http://localhost:8000/api/watercard/";
    return this.http.post(this.url_api + "watercard/", {
      cliente: _idCliente,
      codigo: codigo,
      saldo: saldo
    }, {headers: this.headers}
    ).pipe(map(data => data));
  }

  putWaterCard(_id: number, cliente: number, saldo: number) {
    //const url_api_watercard = "http://localhost:8000/api/watercard/";
    //return this.http.put(url_api_watercard + `${_id}/`, {headers: this.headers});
    return this.http.put(this.url_api + "watercard/" + `${_id}/`, {
      codigo: _id,
      cliente: cliente,
      saldo: saldo
    }, {headers: this.headers}
    ).pipe(map(data => data));
  }

  /*************************  RUTAS PARA VALOR DE CREDITOS *******************************/
  getAllPrices() {
    //const url_api_stock = "http://localhost:8000/api/tipopago/";
    return this.http.get(this.url_api + "tipopago/", {headers: this.headers});
  }

  /*************************  RUTAS PARA CONSIGNAS *******************************/
  /*getConsignas() {
    const url_apii = "http://localhost:8000/api/consignas/";
    return this.http.get(url_apii, {headers: this.headers});
  }

  postConsigna(detalle_consigna: [], repartidor: number, ruta: string, estatus: string, importe: number) {
    const url_apii = "http://localhost:8000/api/consignas/";
    return this.http.post<Sales>(url_apii, {
      detalle_consigna: detalle_consigna,
      repartidor: repartidor,
      ruta: ruta,
      estatus: estatus,
      importe: importe
    }, {headers: this.headers}
    ).pipe(map(data => data));
  }

  updateConsigna(_id: number, detalle_consigna: [], repartidor: number, ruta: string, estatus: string, importe: number) {
    const url_apii = "http://localhost:8000/api/consignas/" + `${_id}/`;
    return this.http.put<Sales>(url_apii, {
      id: _id,
      detalle_consigna: detalle_consigna,
      repartidor: repartidor,
      ruta: ruta,
      estatus: estatus,
      importe: importe
    }, {headers: this.headers}
    ).pipe(map(data => data));
  }*/

  getConsignas() {
    return this.http.get(this.url_api + "consignas/", {headers: this.headers});
  }

  postConsigna(detalle_consigna: any = [], repartidor: number, ruta: string, estatus: string, importe: number) {

    return this.http.post<Sales>(this.url_api + "consignas/", {
      detalle_consigna: detalle_consigna,
      repartidor: repartidor,
      ruta: ruta,
      estatus: estatus,
      importe: importe
    }, {headers: this.headers}
    ).pipe(map(data => data));
  }

  postConsignaNew(detalle_consigna: [], metodo_pago: any = [], repartidor: number, ruta: string, estatus: string, importe: number) {

    return this.http.post<Sales>(this.url_api + 'consignas/', {
      detalle_consigna: detalle_consigna,
      repartidor: repartidor,
      ruta: ruta,
      estatus: estatus,
      importe: importe,
      metodo_pago
    }, {headers: this.headers}
    ).pipe(map(data => data));
  }

  updateConsigna(_id: number, detalle_consigna: [], repartidor: number, ruta: string, estatus: string, importe: number,  edicion: number) {
    return this.http.put<Sales>(this.url_api + "consignas/" + `${_id}/`, {
      id: _id,
      detalle_consigna: detalle_consigna,
      repartidor: repartidor,
      ruta: ruta,
      estatus: estatus,
      importe: importe,
      edicion: edicion
    }, {headers: this.headers}
    ).pipe(map(data => data));
  }

  getCurrentEmployee() {
    return this.http.get(this.url_api + "empleadoactual/", {headers: this.headers});
  }
}
