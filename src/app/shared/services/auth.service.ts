import { UserInterface } from './../../models/user-interface';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }
  headers : HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });

  loginUser(username: string, password: string): Observable<any>{
    const url_api = "https://sistema.waterworld.com.mx/api/token/";
    return this.http.post<UserInterface>(url_api, {
      username: username,
      password: password
    }, { headers: this.headers}
    ).pipe(map(data => data));
  }

  setUser(user: UserInterface): void {
    let user_string = JSON.stringify(user);
    localStorage.setItem("currentUser", user_string);
  }

  setToken(token): void {
    localStorage.setItem("accessToken", token);
    sessionStorage.setItem("accessToken", token);
  }

  getToken(){
    return sessionStorage.getItem("accessToken");
  }

  getCurrentUser(): UserInterface {
    let user_string = localStorage.getItem("currentUser");
    if(!isNullOrUndefined(user_string)){
      let user: UserInterface = JSON.parse(user_string );
      return user;
    }else{
      return null;
    }
  }

  logoutUser(){
    //let accessToken = localStorage.getItem("accessToken")
    //const url_api = `http:/localhost:3000/api/users/logout?access_token=${accessToken}`;
    localStorage.removeItem("accessToken");
    localStorage.removeItem("currentUser");
    sessionStorage.removeItem("accessToken");
    //return this.http.post<UserInterface>(url_api, { headers: this.headers})
  }
}
